#!/usr/bin/env Rscript
library(dplyr)
load("./output/ecospat_climgrid_spp.rdata")

#indexes <- expand.grid(1:length(pca.grid), 1:length(pca.grid))
#indexes_f <- indexes %>% filter(Var1 != Var2)
indexes_f <- data.frame("Var1" = 0, "Var2" = 0)
k <- 1
for (i in (1:length(pca.grid))) {
    for (j in setdiff(i:length(pca.grid), i)){
        indexes_f[k,1] <- i
        indexes_f[k,2] <- j
        k <- k + 1
    }
}
#quais <- 1:nrow(indexes_f)
#indexes_f <- indexes_f[quais,]
z1_list <- pca.grid[indexes_f$Var2]
z2_list <- pca.grid[indexes_f$Var1]
rm(pca.grid)
#el df con la info inicial
sp <- indexes_f[,c(1,2)]#era 2, 1 mas já não é mais necessário inverter porque expand.grid inverte mas meu loop não
sp <- setNames(object = sp, c("Sp.1","Sp.2"))

message("cluster init...")
library(parallel)
library(doParallel)
library(foreach)
core <- detectCores()

source("R/eq_sim_functions.R")

#inicia el cluster
cl <- parallel::makeCluster(120,
                            outfile =
                                paste("eq_ALL_lower.log"), type = "SOCK")
registerDoParallel(cl)
message(paste("registercluster OK", getDoParWorkers()))

(ini = Sys.time())
a <- foreach(lista1 = z1_list,
             lista2 = z2_list,
             .packages = c("ecospat"),
             .export = "ejecutar_Niche_eq") %dopar%
    ejecutar_Niche_eq(z1 = lista1,
                      z2 = lista2,
                      rep = 100,
                      alternative = "lower")
Sys.time() - ini
stopCluster(cl)
df_results <- bind_rows(a)
df_final <- cbind(sp, df_results)
write.csv(df_final, file = "01_results_niche_eq_lower.csv")#was results_niche_eq
