#!/usr/bin/env Rscript
library(dplyr)
load("./output/pca_grid.rdata")

indexes <- expand.grid(1:length(pca.grid), 1:length(pca.grid))
indexes_f <- indexes %>% filter(Var1 != Var2)
#quais <- 1:3
#indexes_f <- indexes_f[quais,]
z1_list <- pca.grid[indexes_f$Var2]
z2_list <- pca.grid[indexes_f$Var1]

rm(pca.grid)
#el df con la info inicial
sp <- indexes_f[,c(2,1)]
sp <- setNames(object = sp, c("Sp.1","Sp.2"))
message("cluster init...")
library(parallel)
library(doParallel)
library(foreach)
core <- detectCores()

# la funcion para que haga la tabla
source("R/eq_sim_functions.R")

#inicia el cluster
cl <- parallel::makeCluster(120,
                            outfile =
                                paste("niche_sim_greater.log"), type = "SOCK")
registerDoParallel(cl)
message(paste("registercluster OK", getDoParWorkers()))

(ini = Sys.time())
a <- foreach(lista1 = z1_list,
             lista2 = z2_list,
             .packages = c("ecospat"),
             .export = "ejecutar_Niche_sim") %dopar%
    ejecutar_Niche_sim(z1 = lista1,
                      z2 = lista2,
                      rep = 100,
                      alternative = "greater")
Sys.time() - ini
stopCluster(cl)
df_results <- bind_rows(a)
df_final <- cbind(sp, df_results)
write.csv(df_final, file = "04_results_niche_sim_greater.csv")
